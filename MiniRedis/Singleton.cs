﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniRedis
{
    /// <summary>
    /// Implementa o Pattern singleton como uma classe generica
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T> where T : class, new()
    {
        private static readonly T instance = new T();
        private Singleton() { }

        /// <summary>
        /// retorna a estancia da classe
        /// </summary>
        public static T GetInstance
        {
            get
            {
                return instance;
            }
        }
    }
}
