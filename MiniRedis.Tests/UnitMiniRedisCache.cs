using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;

namespace MiniRedis.Tests
{
    [TestClass]
    public class UnitMiniRedisCache
    {
        private MiniRedisCache cache = Singleton<MiniRedisCache>.GetInstance;

        [TestMethod]
        public void DbSizeTest()
        {
            Assert.AreEqual(cache.dbsize(), 0);

            for(var i=0;i<10;i++)
                cache.set($"key{i}", $"valor{i}");

            Assert.AreEqual(cache.dbsize(), 10);
            cache.zadd("numbers", 1, "um");
            cache.zadd("numbers", 2, "dois");
            cache.zadd("numbers", 3, "tres");
            cache.zadd("numbers", 4, "quatro");

            Assert.AreEqual(cache.dbsize(), 11);
        }

        [TestMethod]
        public void zAdd_Rank_CardTest()
        {
            Assert.AreEqual(cache.zcard("cores"), 0);

            Assert.IsNull(cache.zrank("cores", "vermelho"));

            cache.zadd("cores", 1, "azul");
            cache.zadd("cores", 2, "vermelho");
            cache.zadd("cores", 3, "preto");
            cache.zadd("cores", 4, "verde");

            cache.zadd("numeros", 1, "um");
            cache.zadd("numeros", 2, "dois");

            Assert.AreEqual(cache.zcard("cores"), 4);
            Assert.AreEqual(cache.zcard("numeros"), 2);

            Assert.AreEqual(cache.zrank("cores", "vermelho"),1);
            Assert.AreNotEqual(cache.zrank("cores", "preto"), 3);

            //valida troca de score no member
            Assert.AreEqual(cache.zrank("cores", "verde"), 3);
            cache.zadd("cores", 2, "verde");
            Assert.AreNotEqual(cache.zrank("cores", "verde"), 3);
            Assert.AreEqual(cache.zrank("cores", "verde"), 2);

            Assert.AreEqual(cache.zrank("numeros", "um"), 0);
            Assert.IsNull(cache.zrank("numeros", "tres"));

            //valida  zrange trazendo todos registros
            var total = cache.zrange("cores", 0, -1).Count;
            Assert.AreEqual(total, 4);

            Assert.AreEqual(cache.zrange("cores", 2, 3).Count, 2);
            //valida a pesquisa pelo antepenultimo membro
            Assert.AreEqual(cache.zrange("cores", 0, -2).Last().Value, cache.zrange("cores", 0, -1)[total - 2].Value);
        }

        [TestMethod]
        public void GetSetTest()
        {
            Assert.AreNotEqual(cache.get("key1"), "valor1");

            cache.set("key1", "valor1");
            Assert.AreEqual(cache.get("key1"), "valor1");
            cache.set("key2", "valor2");
            Assert.AreEqual(cache.get("key2"), "valor2");
            cache.set("key3", "valor3");
            Assert.AreEqual(cache.get("key3"), "valor3");

            cache.set("key3", "valor3.1");
            Assert.AreNotEqual(cache.get("key3"), "valor3");
            Assert.AreEqual(cache.get("key3"), "valor3.1");
        }

        [TestMethod]
        public void DeleteTest()
        {
            Assert.AreEqual(cache.get("key1"), "valor1");
            Assert.IsTrue(cache.del("key1"));
            Assert.AreNotEqual(cache.get("key1"), "valor1");
            Assert.IsFalse(cache.del("key1"));

        }

        [TestMethod]
        public void GetSetExpireTest()
        {

            cache.set("key11", "valor1",5);
            Assert.AreEqual(cache.get("key11"), "valor1");
            Thread.Sleep(TimeSpan.FromSeconds(6));
            Assert.AreNotEqual(cache.get("key11"), "valor1");
        }

        [TestMethod]
        public void IncrTest()
        {
            Assert.AreEqual(cache.incr("keyinc1"), 1);
            Assert.AreEqual(cache.get("keyinc1"), "1");
            Assert.AreEqual(cache.incr("keyinc1"),2);
            Assert.AreEqual(cache.incr("keyinc1"), 3);
            cache.set("keyinc1","5");
            Assert.AreEqual(cache.incr("keyinc1"), 6);
            cache.set("keyinc2", "A5b7");
            try
            {
                cache.incr("keyinc2");
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidCastException));
            }

        }

    }
}
