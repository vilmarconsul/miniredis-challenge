﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniRedis
{
    /// <summary>
    /// Classe de transporte de objetos em cache
    /// </summary>
    public class CacheItem
    {
        public DateTime? expiration { get; set; }
        public string value { get; set; }
    }
}
