﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace MiniRedis
{
    public partial class MiniRedisCache
    {

        private long _dbsizeset()
        {
            lock (_lockerset)
            {
                return _dicCacheSet.SelectMany(x=>x.Key).LongCount();
            }
        }
        public void zadd(string key, int score, string member)
        {
            Dictionary<string, int> zlistMembers; 
            int scoreTemp; 
            try
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentNullException("Invalid key!");
                if (string.IsNullOrEmpty(member))
                    throw new ArgumentNullException("Invalid member!");

                lock (_lockerset)
                {
                    // recupera a lista de members apartir da chave
                    if (!_dicCacheSet.TryGetValue(key, out zlistMembers))
                    {
                        // caso nao encontre a chave, inicializa uma lista
                        zlistMembers = new Dictionary<string, int>()
                        {
                            {member, score }
                        };
                        //adiciona a chave no dictionary os novos valores
                        _dicCacheSet.Add(key, zlistMembers);

                        return;
                    }

                    //recupera o member para atualizar o score
                    if (!zlistMembers.TryGetValue(member, out scoreTemp))
                        //caso nao encontre o member adicina novo item na lista
                        zlistMembers.Add(member, score);
                    else // caso o member exista, apenas atualiza o score
                        zlistMembers[member] = score;

                    //atualiza lista de members na key
                    _dicCacheSet[key] = zlistMembers;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<KeyValuePair<int, string>> zrange(string key, int start=0, int stop = -1)
        {
            Dictionary<string, int> zlistMembers;
            try
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentNullException("Invalid key!");

                // retorna lista vazia em caso de out of range
                if (start < 0 || stop < -2 || (stop >= 0 && start > stop))
                    return new List<KeyValuePair<int, string>>();

                lock (_lockerset)
                {
                    // recupera a lista de members apartir da chave
                    if (!_dicCacheSet.TryGetValue(key, out zlistMembers))
                        return null;
                }
                int indexTmp = 0;
                int endrange = stop; 
                // ordena a lista pelo score e cria um indice base 0
                var tmpmembers = zlistMembers
                        .OrderBy(x => x.Value)
                        .Select(x => new
                        {
                            index = indexTmp++,
                            x.Value,
                            x.Key
                        })
                        .ToList(); 

                // trata para recuperar até o ultimo registro (-1) ou antepenultimo (-2)
                if (stop < 0)
                    endrange = tmpmembers.Count() + stop;

                //retorna o range de members e scores
                return tmpmembers
                    .Where(x => x.index >= start && x.index <= endrange)
                    .Select(f=> new KeyValuePair<int, string>(f.Value,f.Key))
                    .ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int zcard(string key)
        {
            Dictionary<string, int> zlistMembers;
            try
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentNullException("Invalid key!");

                lock (_lockerset)
                {
                    // recupera a lista de members apartir da chave
                    if (!_dicCacheSet.TryGetValue(key, out zlistMembers))
                        return 0;
                }

                return zlistMembers.Count();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int? zrank(string key, string member)
        {
            Dictionary<string, int> zlistMembers;
            try
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentNullException("Invalid key!");

                lock (_lockerset)
                {
                    // recupera a lista de members apartir da chave
                    if (!_dicCacheSet.TryGetValue(key, out zlistMembers))
                        return null;
                }

                //testa se o member existe
                if (zlistMembers.Where(x => x.Key == member).Count() == 0)
                    return null;

                //efetua a ordenação pelo score e criação do indice base 0
                int indexTmp = 0;
                var tmpmembers = zlistMembers
                    .OrderBy(x => x.Value)
                    .Select(x => new
                    {
                        index = indexTmp++,
                        x.Value,
                        x.Key
                    })
                    .ToList();

                //retorna o score do member
                return tmpmembers.Where(x => x.Key == member)
                                 .Select(x => x.index)
                                 .First();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

}
