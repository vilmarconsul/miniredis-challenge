﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MiniRedis.Service.Controllers
{
    /// <summary>
    /// Interface API Rest MiniRedis
    /// </summary>
    [Produces("application/json")]
    [Route("[controller]")]
    public class ApiController : Controller
    {

        private MiniRedisCache _cache;
        /// <summary>
        /// Ctor, recebe o MiniRedis do IOC
        /// </summary>
        /// <param name="cache"></param>
        public ApiController(MiniRedisCache cache)
        {
            _cache = cache;
        }

        /// <summary>
        /// Implementa DBSIZE
        /// </summary>
        /// <returns>quantidade de registros</returns>
        [HttpGet("/Api/dbsize")]
        [HttpGet("/cmd DBSIZE")]
        public IActionResult Dbsize()
        {
            try
            {
                return new ObjectResult(_cache.dbsize());
            }
            catch (Exception ex)
            {
                return new ObjectResult((ex.InnerException ?? ex).Message)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
            
        }
        /// <summary>
        /// Implementa metodo GET
        /// </summary>
        /// <param name="key">chave</param>
        /// <returns>retorna o valor da chave ou nulo</returns>
        [HttpGet("{key}", Name = "Get")]
        [HttpGet("/cmd GET {key}")]
        public IActionResult Get(string key)
        {
            try
            {
                return new ObjectResult(_cache.get(key));
            }
            catch (Exception ex)
            {
                return new ObjectResult((ex.InnerException ?? ex).Message)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                }; 
            }
        }
        /// <summary>
        /// Implementa metodo SET
        /// </summary>
        /// <param name="key">chave</param>
        /// <returns>OK para registro salvo com sucesso</returns>
        [HttpPut("{key}")]
        [Consumes("text/plain")]
        public IActionResult Put(string key)
        {
            try
            {
                var body = Request.Body.ToText();

                _cache.set(key, body);
                return new ObjectResult("OK");
            }
            catch (Exception ex)
            {
                return new ObjectResult((ex.InnerException ?? ex).Message)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
        }

        /// <summary>
        /// Implementa metodo SET
        /// </summary>
        /// <param name="key">chave</param>
        /// <param name="value">valor</param>
        /// <param name="expiration">expiração em segundos</param>
        /// <returns>OK para registro salvo com sucesso</returns>
        [HttpPut("/Api/set/{key} {value} {expiration}")]
        [HttpGet("/cmd SET {key} {value} {expiration}")]
        [Consumes("text/plain")]
        public IActionResult set(string key, string value, int expiration)
        {
            try
            {
                _cache.set(key, value, expiration);
                return new ObjectResult("OK");
            }
            catch (Exception ex)
            {
                return new ObjectResult((ex.InnerException ?? ex).Message)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
        }


        /// <summary>
        /// Implementa metodo INCR
        /// </summary>
        /// <param name="key">chave</param>
        /// <returns>numero inteiro incrementado, caso nao exista a chave retorna 1. Caso o valor armazenado não seja um int, retorna null</returns>
        [HttpPost("/Api/incr/{key}")]
        [HttpGet("/cmd INCR {key}")]
        [Consumes("text/plain")]
        public IActionResult Incr(string key)
        {
            try
            {

                return new ObjectResult(_cache.incr(key));
            }
            catch (Exception ex)
            {
                return new ObjectResult((ex.InnerException ?? ex).Message)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
        }

        /// <summary>
        /// Implementa metodo DEL
        /// </summary>
        /// <param name="key">chave</param>
        /// <returns>OK para registro deletado, NO para chave inexistente</returns>
        [HttpDelete("{key}")]
        [HttpGet("/cmd DEL {key}")]
        public IActionResult Delete(string key)
        {
            try
            {
                return new ObjectResult((_cache.del(key)) ? "OK" : "NO" );
            }
            catch (Exception ex)
            {
                return new ObjectResult((ex.InnerException ?? ex).Message)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
        }

        /// <summary>
        /// Implementa metodo ZCARD
        /// </summary>
        /// <param name="key">chave</param>
        /// <returns></returns>
        [HttpGet("/Api/zcard/{key}", Name = "zcard")]
        [HttpGet("/cmd ZCARD {key}")]
        public IActionResult zcard(string key)
        {
            try
            {
                return new ObjectResult(_cache.zcard(key));
            }
            catch (Exception ex)
            {
                return new ObjectResult((ex.InnerException ?? ex).Message)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
        }

        /// <summary>
        /// Implementa metodo ZRANK
        /// </summary>
        /// <param name="key">chave</param>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpGet("/Api/zrank/{key}/{member}", Name = "zrank")]
        [HttpGet("/cmd ZRANK {key} {member}")]
        public IActionResult zrank(string key, string member)
        {
            try
            {
                return new ObjectResult(_cache.zrank(key, member));
            }
            catch (Exception ex)
            {
                return new ObjectResult((ex.InnerException ?? ex).Message)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
        }

        /// <summary>
        /// Implementa metodo ZRANGE
        /// </summary>
        /// <param name="key">chave</param>
        /// <param name="start">indice inicial</param>
        /// <param name="stop">indice final. -1 -> retorna todos registros , -2 -> retorna até o antepenultimo</param>
        /// <returns></returns>
        [HttpGet("/Api/zrange/{key}/{start} {stop}", Name = "zrange")]
        [HttpGet("/cmd ZRANGE {key} {start} {stop}")]
        public IActionResult zrange(string key, int start, int stop)
        {
            try
            {
                return new ObjectResult(_cache.zrange(key, start, stop));
            }
            catch (Exception ex)
            {
                return new ObjectResult((ex.InnerException ?? ex).Message)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
        }

        /// <summary>
        /// Implementa metodo SET
        /// </summary>
        /// <param name="key">chave</param>
        /// <param name="score"></param>
        /// <param name="member"></param>
        /// <returns>OK para registro salvo com sucesso</returns>
        [HttpPut("/Api/zadd/{key} {score} {member}")]
        [HttpGet("/cmd ZADD {key} {score} {member}")]
        [Consumes("text/plain")]
        public IActionResult zadd(string key, int score, string member)
        {
            try
            {
                _cache.zadd(key, score, member);
                return new ObjectResult("OK");
            }
            catch (Exception ex)
            {
                return new ObjectResult((ex.InnerException ?? ex).Message)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
        }

    }
}
