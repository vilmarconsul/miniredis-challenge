﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace MiniRedis
{
    /// <summary>
    /// Implementação MiniRedis
    /// </summary>
    public partial class MiniRedisCache 
    {
        //locker object para tratar concorrencia ao dicionario _dicCacheSet
        private static object _lockerset = new object();
        //locker object para tratar concorrencia ao dicionario _dicCache
        private static object _locker = new object();
        //armazena a estrutura de cache de itens individuais
        private Dictionary<string, CacheItem> _dicCache;
        //armazena a estrutura de cache de metodos "z" que manipulam set de itens
        private Dictionary<string, Dictionary<string, int>> _dicCacheSet;

        public MiniRedisCache()
        {
            _dicCache = new Dictionary<string, CacheItem>();
            _dicCacheSet = new Dictionary<string, Dictionary<string, int>>();
        }

        public long dbsize()
        {
            var sizeset = _dbsizeset();
            lock (_locker)
            {
                return _dicCache.Where(x => !x.Value.expiration.HasExpired()).LongCount() + sizeset;
            }
        }

        public bool del(string key)
        {
            try
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentNullException("Invalid key!");

                lock (_locker)
                {
                    return _dicCache.Remove(key);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string get(string key)
        {
            try
            {
                lock (_locker)
                {
                    return _get(key);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public long incr(string key)
        {
            var value = 0L;
            try
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentNullException("Invalid key!");

                lock (_locker)
                {
                    var item = this._get(key);
                    // verifica se o item existe
                    if (!string.IsNullOrEmpty(item))
                        //testa um cast para long do valor armazenado
                        if (!long.TryParse(item, out value))
                            throw new InvalidCastException("This operation is limited to 64 bit signed integers");

                    //efetua o incremento e salva o item
                    this._set(key, (++value).ToString());
                }

                return value;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void set(string key, string value, int expiration = 0)
        {
            try
            {
                lock (_locker)
                {
                    _set(key, value, expiration);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region methods non threadsafe
        private void _set(string key, string value, int expiration = 0)
        {
            try
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentNullException("Invalid key!");
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("Invalid value!");
                //se a expiração for invalida ajusta para sem expiração.
                if (expiration < 0)
                    expiration = 0;

                var newitem = new CacheItem() { value = value };
                // calcula a expiração do item
                if (expiration > 0)
                    newitem.expiration = DateTime.Now.AddSeconds(expiration);

                //insere ou atualiza a chave utilizando a extension
                if (!_dicCache.TryAdd(key, newitem))
                    _dicCache[key] = newitem;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string _get(string key)
        {
            try
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentNullException("Invalid key!");

                CacheItem item;
                //tenta recupear o item
                if (_dicCache.TryGetValue(key, out item))
                {
                    //testa se o item expirou e o deleta
                    if (item.expiration.HasExpired())
                    {
                        _dicCache.Remove(key);
                        return null;
                    }

                    return item.value;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return null;
        }
        #endregion

    }


}
