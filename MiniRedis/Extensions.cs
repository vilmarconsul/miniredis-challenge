﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MiniRedis
{
    /// <summary>
    /// Metodos de extensão
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Verifica se uma data já expirou
        /// </summary>
        /// <param name="value">data a ser comparada com DateTime.Now</param>
        /// <returns></returns>
        public static bool HasExpired(this DateTime? value)
        {
            return (value.HasValue) 
                ? (value.Value < DateTime.Now) 
                : false;
        }
        /// <summary>
        /// Converte um stream para string
        /// </summary>
        /// <param name="value">stream</param>
        /// <returns>string</returns>
        public static string ToText(this Stream value)
        {
            using (StreamReader reader = new StreamReader(value, Encoding.UTF8))
            {
                return reader.ReadToEndAsync().Result;
            }

        }
    }
}
